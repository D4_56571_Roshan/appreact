const express = require('express')

const app = express()

app.get('/', (request, response) => {
  response.send('welcome to the backend')
})

app.get('/test', (request, response) => {
  response.send('new test api introduced')
})

app.get('/test-1', (request, response) => {
  response.send('new test-1 api introduced')
})

app.listen(4000, '0.0.0.0', () => {
  console.log(`server started on port 4000`)
})
